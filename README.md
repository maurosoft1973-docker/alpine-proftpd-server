# ProFTPD Server Docker image running on Alpine Linux

[![Docker Automated build](https://img.shields.io/docker/automated/maurosoft1973/alpine-proftpd-server.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-proftpd-server/)
[![Docker Pulls](https://img.shields.io/docker/pulls/maurosoft1973/alpine-proftpd-server.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-proftpd-server/)
[![Docker Stars](https://img.shields.io/docker/stars/maurosoft1973/alpine-proftpd-server.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-proftpd-server/)

[![Alpine Version](https://img.shields.io/badge/Alpine%20version-v3.17.3-green.svg?style=for-the-badge)](https://alpinelinux.org/)

The Docker images [(maurosoft1973/alpine-proftpd-server)](https://hub.docker.com/r/maurosoft1973/alpine-proftpd-server/) is based on the minimal [Alpine Linux](https://alpinelinux.org/)  with [ProFTPD Version v1.3.8-r4](http://www.proftpd.org/).

##### Alpine Version 3.17.3 (Released Mar 29 2023)
##### ProFTPD Version 1.3.8-r4 (Released 2022-15-05 03:12:37)

## Description

This is image include the ProFTPD server.

The working directory is the one associated with the user (the home directory is /home/{user name}).

For network-wide access, the MasqueradeAddress must be specified (this is the IP of the host machine on which the docker container runs)


## Architectures

* ```:aarch64``` - 64 bit ARM
* ```:armhf```   - 32 bit ARM v6
* ```:armv7```   - 32 bit ARM v7
* ```:ppc64le``` - 64 bit PowerPC
* ```:x86```     - 32 bit Intel/AMD
* ```:x86_64```  - 64 bit Intel/AMD (x86_64/amd64)

## Tags

* ```:latest```         latest branch based (Automatic Architecture Selection)
* ```:aarch64```        latest 64 bit ARM
* ```:armhf```          latest 32 bit ARM v6
* ```:armv7```          latest 32 bit ARM v7
* ```:ppc64le```        latest 64 bit PowerPC
* ```:x86```            latest 32 bit Intel/AMD
* ```:x86_64```         latest 64 bit Intel/AMD
* ```:test```           test branch based (Automatic Architecture Selection)
* ```:test-aarch64```   test 64 bit ARM
* ```:test-armhf```     test 32 bit ARM v6
* ```:test-armv7```     test 32 bit ARM v7
* ```:test-ppc64le```   test 64 bit PowerPC
* ```:test-x86```       test 32 bit Intel/AMD
* ```:test-x86_64```    test 64 bit Intel/AMD
* ```:3.17.3``` 3.17.3 branch based (Automatic Architecture Selection)
* ```:3.17.3-aarch64```   3.17.3 64 bit ARM
* ```:3.17.3-armhf```     3.17.3 32 bit ARM v6
* ```:3.17.3-armv7```     3.17.3 32 bit ARM v7
* ```:3.17.3-ppc64le```   3.17.3 64 bit PowerPC
* ```:3.17.3-x86```       3.17.3 32 bit Intel/AMD
* ```:3.17.3-x86_64```    3.17.3 64 bit Intel/AMD
* ```:3.17.3-1.3.8-r4``` 3.17.3-1.3.8-r4 branch based (Automatic Architecture Selection)
* ```:3.17.3-1.3.8-r4-aarch64```   3.17.3 64 bit ARM
* ```:3.17.3-1.3.8-r4-armhf```     3.17.3 32 bit ARM v6
* ```:3.17.3-1.3.8-r4-armv7```     3.17.3 32 bit ARM v7
* ```:3.17.3-1.3.8-r4-ppc64le```   3.17.3 64 bit PowerPC
* ```:3.17.3-1.3.8-r4-x86```       3.17.3 32 bit Intel/AMD
* ```:3.17.3-1.3.8-r4-x86_64```    3.17.3 64 bit Intel/AMD

## Layers & Sizes

| Version                                                                               | Size                                                                                                                 |
|---------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
| ![Version](https://img.shields.io/badge/version-amd64-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-proftpd-server/latest?style=for-the-badge)  |
| ![Version](https://img.shields.io/badge/version-armv6-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-proftpd-server/armhf?style=for-the-badge)   |
| ![Version](https://img.shields.io/badge/version-armv7-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-proftpd-server/armv7?style=for-the-badge)   |
| ![Version](https://img.shields.io/badge/version-ppc64le-blue.svg?style=for-the-badge) | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-proftpd-server/ppc64le?style=for-the-badge) |
| ![Version](https://img.shields.io/badge/version-x86-blue.svg?style=for-the-badge)     | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-proftpd-server/x86?style=for-the-badge)     |

## Environment Variables:

### Main ProFTPD parameters:
* `LC_ALL`: default locale (default en_GB.UTF-8)
* `TIMEZONE`: default timezone (default Europe/Brussels)
* `SHELL_TERMINAL`: default shell (bin/sh)
* `FTP_SERVER_NAME`: name of server (default ProFTPD Server)
* `FTP_PORT`: ftp port (default 21)
* `FTP_USER`: user ftp (default demo)
* `FTP_USER_PASSWORD`: password user ftp (default rhrRV6b8mA)
* `MAX_INSTANCES`: the maximum number of child processes (default 30)
* `ALLOW_OVERWRITE`: enabled write (default on)
* `PASV_MIN_PORT`: min passive port (default 31000)
* `PASV_MAX_PORT`: max passive port (default 31003)
* `MASQUERADEADDRESS`: The MasqueradeAddress directive causes the server to display the network information for the specified IP address or DNS hostname to the client in the responses to PASV and EPSV FTP commands, on the assumption that that IP address or DNS host is acting as a NAT gateway or port forwarder for the server

## Sample Use with gitlab pipeline

### 1. Used as a service, within a job, to put file with default value
```yalm
ftp_upload:
    image: maurosoft1973/alpine
    services:
        - name: maurosoft1973/alpine-proftpd-server
          alias: proftpd
    script:
        - apk add --no-cache ncftp
        - |
          touch upload.txt
          ncftpput -u demo -p demo proftpd /home/demo upload.txt
```

### 2. Run a container docker network-wide access
```yalm
docker run --rm --name ftp_server \
       --env="MASQUERADEADDRESS=192.168.1.1" \
       -p 20-21:20-21 \
       -p 31000-31003:31000-31003 \
       maurosoft1973/alpine-proftpd-server
```

***
###### Last Update 13.09.2023 14:30:29
