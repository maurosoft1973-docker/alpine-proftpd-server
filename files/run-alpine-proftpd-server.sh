#!/bin/bash
source /scripts/init-alpine.sh

set -e # exit on error
export FTP_SERVER_NAME=${FTP_SERVER_NAME:-"ProFTPD Server"}
export FTP_PORT=${FTP_PORT:-21}
export FTP_USER=${FTP_USER:-"demo"}
FTP_USER_PASSWORD=${FTP_USER_PASSWORD:-"rhrRV6b8mA"}
export MAX_INSTANCES=${MAX_INSTANCES:-30}
export ALLOW_OVERWRITE=${ALLOW_OVERWRITE:-"on"}
export PASV_MIN_PORT=${PASV_MIN_PORT:-31000}
export PASV_MAX_PORT=${PASV_MAX_PORT:-31003}
export MASQUERADEADDRESS=${MASQUERADEADDRESS:-$IP}
export SYS_LOGLEVEL=${SYS_LOGLEVEL:-"debug"}
DEBUG=${DEBUG:-"0"}

if [ $DEBUG == 1 ]; then
    echo "FTP_SERVER_NAME   -> $FTP_SERVER_NAME"
    echo "FTP_PORT          -> $FTP_PORT"
    echo "FTP_USER          -> $FTP_USER"
    echo "FTP_USER_PASSWORD -> $FTP_USER_PASSWORD"
    echo "MAX_INSTANCES     -> $MAX_INSTANCES"
    echo "ALLOW_OVERWRITE   -> $ALLOW_OVERWRITE"
    echo "PASV_MIN_PORT     -> $PASV_MIN_PORT"
    echo "PASV_MAX_PORT     -> $PASV_MAX_PORT"
    echo "MASQUERADEADDRESS -> $MASQUERADEADDRESS"
    echo "SYS_LOGLEVEL      -> $SYS_LOGLEVEL"
fi

echo -e "Add group ftpgroups"
addgroup ftpgroups

echo -e "Create User $FTP_USER -> /home/$FTP_USER"
adduser -h /home/$FTP_USER -s /bin/sh -D $FTP_USER
adduser $FTP_USER ftpgroups
echo -n "$FTP_USER:$FTP_USER_PASSWORD" | chpasswd
chown $FTP_USER:$FTP_USER /home/$SSH_USER
chmod 755 /home/$FTP_USER

# Template
export DOLLAR='$'
envsubst < /root/config/proftpd.conf > /etc/proftpd/proftpd.conf
envsubst < /root/config/custom.conf > /etc/proftpd/custom.conf

if [ "$DEBUG" == 1 ]; then
    echo "Display content of /etc/proftpd/proftpd.conf"
    cat /etc/proftpd/proftpd.conf

    echo "Display content of /etc/proftpd/custom.conf"
    cat /etc/proftpd/custom.conf
fi

exec proftpd --nodaemon -c /etc/proftpd/proftpd.conf