ARG DOCKER_ALPINE_VERSION

FROM maurosoft1973/alpine:$DOCKER_ALPINE_VERSION

ARG BUILD_DATE
ARG ALPINE_ARCHITECTURE
ARG ALPINE_RELEASE
ARG ALPINE_VERSION
ARG ALPINE_VERSION_DATE
ARG PROFTPD_VERSION
ARG PROFTPD_VERSION_DATE

LABEL \
    maintainer="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    architecture="$ALPINE_ARCHITECTURE" \
    proftpd-server-version="$PROFTPD_SERVER_VERSION" \
    alpine-version="$ALPINE_VERSION" \
    build="$BUILD_DATE" \
    org.opencontainers.image.title="alpine-proftpd-server" \
    org.opencontainers.image.description="ProFTPD Server $PROFTPD_SERVER_VERSION Docker image running on Alpine Linux" \
    org.opencontainers.image.authors="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    org.opencontainers.image.vendor="Mauro Cardillo" \
    org.opencontainers.image.version="v$PROFTPD_SERVER_VERSION" \
    org.opencontainers.image.url="https://hub.docker.com/r/maurosoft1973/alpine-proftpd-server" \
    org.opencontainers.image.source="https://gitlab.com/maurosoft1973-docker/alpine-proftpd-server" \
    org.opencontainers.image.created=$BUILD_DATE

RUN \
    echo "" > /etc/apk/repositories && \
    echo "https://dl-cdn.alpinelinux.org/alpine/v$ALPINE_RELEASE/main" >> /etc/apk/repositories && \
    echo "https://dl-cdn.alpinelinux.org/alpine/v$ALPINE_RELEASE/community" >> /etc/apk/repositories && \
    apk update && \
    apk add --update --no-cache proftpd gettext && \
    mkdir /var/proftpd/ && \
    mkdir /run/proftpd/ && \
    cp /usr/bin/envsubst /usr/local/bin/

COPY config/ /root/config
COPY files/ /scripts
RUN chmod +x /scripts/run-alpine-proftpd-server.sh

RUN ln -sf /dev/stderr /var/log/proftpd/stdout.log

EXPOSE 20 21

ENTRYPOINT ["/scripts/run-alpine-proftpd-server.sh"]
